<?php

namespace App\Tests;

final class StoryTest extends AbstractTest
{
    public function testCreatStory(): void
    {
        $response = $this->createClientWithCredentials()->request('POST', '/api/stories', [
            'json' => [
                "title" => "My Title",
                "description" => "My description"
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['title' => 'My Title']);
        $this->assertJsonContains(['user' => ['email' => 'test@free.fr']]);
    }
}
