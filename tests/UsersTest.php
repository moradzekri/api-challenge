<?php

namespace App\Tests;

final class UsersTest extends AbstractTest
{
    public function testListUsers(): void
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/users');
        $this->assertResponseIsSuccessful();
    }
}
