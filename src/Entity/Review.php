<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ORM\Entity(repositoryClass: ReviewRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get",
        "post",
    ],
    itemOperations: [
        "delete" => ["security" => "object.getUser() === user"],
        "get",
        "put" => ["security" => "object.getUser() === user"],
    ],
    denormalizationContext: ['groups' => ['write']],
    normalizationContext: ['groups' => ['read']],
)]
class Review
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['write', 'read'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "My review",
            ],
        ],
    )]
    private ?string $title;

    #[ORM\ManyToOne(targetEntity: Story::class, inversedBy: 'reviews')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "IRI",
                "example" => "/api/stories/1"
            ],
        ]
    )]
    #[MaxDepth(1)]
    #[Groups(['write'])]
    private ?Story $story;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'reviews')]
    #[Groups(['write', 'read'])]
    #[MaxDepth(1)]
    private ?User $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStory(): ?Story
    {
        return $this->story;
    }

    public function setStory(?Story $story): self
    {
        $this->story = $story;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
