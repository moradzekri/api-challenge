<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\StoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: StoryRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get",
        "post",
    ],
    itemOperations: [
        "delete" => ["security" => "object.getUser() == user"],
        "get",
        "put" => ["security" => "object.getUser() === user"],
        'get_story_average' => ['route_name' => 'stories_average'],
    ],
    denormalizationContext: ['groups' => ['write']],
    normalizationContext: ['groups' => ['read']]
)]
#[ApiFilter(SearchFilter::class, properties: [
    'id' => 'exact',
    'title' => 'partial',
    'description' => 'partial'
])]
#[ApiFilter(OrderFilter::class, properties: ['id' => 'DESC'])]
class Story
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['write', 'read'])]
    private ?int  $id = null;

    #[ORM\Column(type: 'string', length: 115)]
    #[Groups(['write', 'read'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "My Title",
            ],
        ],
    )]
    #[Assert\NotNull]
    #[Assert\Length(min: 3, max: 50)]
    private ?string $title;

    #[ORM\Column(type: 'text')]
    #[Groups(['write', 'read'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "text",
                "example" => "My description",
            ],
        ],
    )]
    #[Assert\NotNull]
    #[Assert\Length(min: 8)]
    private ?string $description;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'stories')]
    #[Groups(['read'])]
    private ?User $user;

    #[ORM\OneToMany(mappedBy: 'story', targetEntity: Review::class)]
    #[MaxDepth(1)]
    #[Groups(['read'])]
    private $reviews;

    #[Pure] public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Review>
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setStory($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getStory() === $this) {
                $review->setStory(null);
            }
        }

        return $this;
    }
}
