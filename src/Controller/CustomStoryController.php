<?php

namespace App\Controller;

use App\Entity\Story;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class CustomStoryController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/stories/{id}/average', name: 'stories_average')]
    public function index(int $id): JsonResponse
    {
        $story = $this->entityManager->getRepository(Story::class)->find($id);

        if (!isset($story)) {
            return  $this->json(
                [
                    'message' => 'Story not found'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json(
            [
                'title' => $story->getTitle(),
                'description' => $story->getDescription(),
                'user' => [
                  'email' => $story->getUser()->getEmail()
                ],
                'average' => count($story->getReviews())
            ],
            Response::HTTP_OK
        );
    }
}
