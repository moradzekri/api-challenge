<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Review;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ReviewDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private TokenStorageInterface $storage,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Review;
    }

    public function persist($data, array $context = []): Review
    {
        /** @var Review $review */
        $review = $data;

        /** @var User $user */
        $user = $this->storage->getToken()->getUser();
        $review->setUser($user);

        $this->entityManager->persist($review);
        $this->entityManager->flush();

        return $review;
    }

    public function remove($data, array $context = [])
    {
        // TODO: Implement remove() method.
    }
}
