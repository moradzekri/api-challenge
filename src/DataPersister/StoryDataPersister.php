<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Story;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class StoryDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private TokenStorageInterface $storage,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Story;
    }

    /**
     * @param $data
     * @param array $context
     * @return Story
     */
    public function persist($data, array $context = []): Story
    {
        /** @var Story $story */
        $story = $data;

        /** @var User $user */
        $user = $this->storage->getToken()->getUser();
        $story->setUser($user);

        $this->entityManager->persist($story);
        $this->entityManager->flush();

        return $story;
    }

    public function remove($data, array $context = [])
    {
        // TODO: Implement remove() method.
    }
}
