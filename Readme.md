<h1 align="center"><a href="https://api-platform.com"><img src="https://api-platform.com/logo-250x250.png" alt="API Platform"></a></h1>

This is Challenge Api Exercice

## Install

to run the application please use this commands

    $ sudo docker-compose up -d
    $ sudo docker-compose exec php-fpm composer install
    $ sudo docker-compose exec php-fpm bin/console doctrine:database:create
    $ sudo docker-compose exec php-fpm bin/console doctrine:migrations:migrate
    $ sudo docker-compose exec php-fpm bin/console doctrine:fixtures:load

Before to use the api you will need a token you can go to this 
link https://localhost:8080/authentication_token with this credentials 

`{"email":"test@free.fr","password":"test"}`

You can now go to https://localhost:8080/api/docs

## CS FIXER

to run the cs clean fix please use this commands

    $ sudo docker-compose exec php-fpm mkdir --parents tools/php-cs-fixer
    $ sudo docker-compose exec php-fpm composer require --working-dir=tools/php-cs-fixer friendsofphp/php-cs-fixer
    $ sudo docker-compose exec php-fpm tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src
    $ sudo docker-compose exec php-fpm tools/php-cs-fixer/vendor/bin/php-cs-fixer fix tests

## Testing

to run the testing please use this commands

    $ sudo docker-compose exec php-fpm composer install
    $ sudo docker-compose exec php-fpm bin/console doctrine:database:create --env=test
    $ sudo docker-compose exec php-fpm bin/console doctrine:migrations:migrate --env=test
    $ sudo docker-compose exec php-fpm bin/console doctrine:fixtures:load --env=test
    $ sudo docker-compose exec php-fpm bin/phpunit
